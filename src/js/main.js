$(document).ready(function () {

    var checkBrowser = function () {
        if (!Modernizr.flexbox && (navigator.appVersion.indexOf("MSIE 10") == -1)) {
            if (!window.XMLHttpRequest && 'ActiveXObject' in window) {
                window.XMLHttpRequest = function () {
                    return new ActiveXObject('MSXML2.XMLHttp');
                };
            }
            var xhr = new XMLHttpRequest();
            xhr.open('GET', '/outdated-browser.php', true);
            xhr.onreadystatechange = function () {
                if (this.readyState !== 4) return;
                if (this.status !== 200) return; // or whatever error handling you want
                document.getElementById('page').innerHTML = this.responseText;
            };
            xhr.send();
        }
    };

    checkBrowser();

    $('body').foundation();

    // Блог преимущества
    $('#progress__toggler').on('click', function (e) {
        $(this).toggleClass('open');

        $('#processor-notation').foundation('toggle');
    });

    // Placeholder
    $('input, textarea').placeholder();

    // Phone mask
    $('.phone').mask("+7(999) 999-9999");

    // Отправка формы
    $('form').on('submit', function (e) {
        e.preventDefault();
        e.stopPropagation();

        var form = $(this);
        var template = {
            success: function (text) {
                text = text || '<p>Спасибо, мы получили Ваш вопрос и ответим на него в течение 1 часа</p>';
                var result = $(
                    '<div style="display: none;" class="success callout form-message" data-closable="slide-out-right">'
                    + text.toString() +
                    '</div>');

                setTimeout(function () {
                    result.hide(300, function () {
                        result.remove();
                    })
                }, 5000);
                return result;
            },
            error: function (text) {
                text = text || '<h5>Упс!</h5><p>Произошла ошибка. Пожалуйста, повторте попытку.</p>';
                var result = $(
                    '<div style="display: none;" class="alert callout form-message">'
                    + text.toString() +
                    '</div>');

                setTimeout(function () {
                    result.hide(300, function () {
                        result.remove();
                    })
                }, 4000);
                return result;

            }
        };
        var showMessage = function (element) {

            form.find('.result-wrap').append(element);

            setTimeout(function () {
                element.show(300)
            }, 100);
        };

        if (form.hasClass('disable')) {
            return
        }


        $.ajax({
            url: form.attr('action'),
            data: form.serialize(),
            dataType: 'JSON',
            beforeSend: function () {
                form.addClass('disable');
            },
            success: function (data) {
                var result = '';

                if (data.errorCode == 0) {
                    result = template.success(data.message);
                } else {
                    result = template.error(data.message);
                }

                showMessage(result);
            },
            complete: function () {
                form.removeClass('disable');
                form.find('input, textarea').not('[type="hidden"]').val('');
            },
            error: function () {
                showMessage(template.error());
            }
        });
    });

    // Небольшая анимация для шапки
    var header = $('.header');

    setInterval(function () {
        header.toggleClass('header--garland-light')
    }, 1000);

    console.log('[App init]');
});