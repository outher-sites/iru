'use strict';

const gulp = require('gulp');
const sass = require('gulp-sass');
const gulpIf = require('gulp-if');
const argv = require('yargs').argv;
const rimraf = require('rimraf');
const sourcemaps = require('gulp-sourcemaps');
const notify = require('gulp-notify');
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const imagemin = require('gulp-imagemin');
const uglify = require('gulp-uglify');
const concat = require('gulp-concat');
const plumber = require('gulp-plumber');
const svgSprite = require('gulp-svg-sprite');
const cssnano = require('cssnano');
const flexfix = require('postcss-flexbugs-fixes');
const colorFunction = require('postcss-color-function');
const spritesmith = require('gulp.spritesmith');


const isDevelopment = !argv.production;
const config = {
    destDir: './',
    sourceDir: 'src'
};
let customFoundationCore = [
    'node_modules/foundation-sites/dist/js/plugins/foundation.core.js',
    'node_modules/foundation-sites/dist/js/plugins/foundation.util.box.js',
    'node_modules/foundation-sites/dist/js/plugins/foundation.util.keyboard.js',
    'node_modules/foundation-sites/dist/js/plugins/foundation.util.motion.js',
    'node_modules/foundation-sites/dist/js/plugins/foundation.util.nest.js',
    'node_modules/foundation-sites/dist/js/plugins/foundation.util.timer.js',
    'node_modules/foundation-sites/dist/js/plugins/foundation.util.touch.js',
    'node_modules/foundation-sites/dist/js/plugins/foundation.util.triggers.js',
    'node_modules/foundation-sites/dist/js/plugins/foundation.smoothScroll.js',
    'node_modules/foundation-sites/dist/js/plugins/foundation.magellan.js',
    'node_modules/foundation-sites/dist/js/plugins/foundation.toggler.js',
];

let conf_postcss = {
    default: [
        colorFunction,
        autoprefixer({browsers: ['last 2 versions', 'ie >= 9']}),
        flexfix
    ],

    prod: [
        colorFunction,
        autoprefixer({browsers: ['last 2 versions', 'ie >= 9']}),
        flexfix,
        cssnano({preset: 'default'})
    ]

};

gulp.task('styles', function () {
    return gulp.src('src/scss/template_styles.scss')
        .pipe(plumber({errorHandler: notify.onError({title: "styles"})}))
        .pipe(gulpIf(isDevelopment, sourcemaps.init()))
        .pipe(sass())
        .pipe(postcss(
            gulpIf(isDevelopment, conf_postcss.default, conf_postcss.prod)
        ))
        .pipe(gulpIf(isDevelopment, sourcemaps.write()))
        .pipe(gulp.dest(config.destDir + '/css/'))
});

gulp.task('clean', function (callback) {
    rimraf('dist', callback);

});

// Создание спрайтов
gulp.task('sprite-create', function () {
    var fileName = 'icons'; //'sprite-' + Math.random().toString().replace(/[^0-9]/g, '');

    var spriteData = gulp.src(config.sourceDir + '/img/sprite/*.png')
        .pipe(spritesmith({
            cssName: '_sprite.scss',
            cssFormat: 'scss',
            cssVarMap: function (sprite) {
                sprite.name = 'icon-' + sprite.name.replace('@', '-');
            },
            imgName: fileName + '.png',
            imgPath: '../img/' + fileName + '.png',
        }));

    spriteData.img
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}]
        }))
        .pipe(gulp.dest(config.destDir + '/img/'));

    spriteData.css
        .pipe(gulp.dest(config.sourceDir + '/scss/components/'));

    return spriteData;
});

gulp.task('java-script', function () {
    return gulp.src([
        "node_modules/jquery/dist/jquery.min.js"].concat(customFoundationCore, [
        "node_modules/jquery-placeholder/jquery.placeholder.js",
        "src/js/jquery.maskedinput.min.js",
        "src/js/modernizr-custom.js",
        "src/js/main.js"
    ]))
        .pipe(plumber({errorHandler: notify.onError({title: "java-script"})}))
        .pipe(gulpIf(isDevelopment, sourcemaps.init()))
        .pipe(concat('bundle.js'))
        .pipe(gulpIf(!isDevelopment, uglify()))
        .pipe(gulpIf(isDevelopment, sourcemaps.write()))
        .pipe(gulp.dest(config.destDir + '/js/'));
});


gulp.task('images', function () {
    return gulp.src(['src/img/*.{jpg,png}', 'src/img/**/*.{jpg,png}', '!src/{img,images}/svg/', '!src/{img,images}/sprite/'])
        .pipe(plumber({errorHandler: notify.onError({title: "images"})}))
        .pipe(gulpIf(!isDevelopment, imagemin({progressive: true})))
        .pipe(gulp.dest(config.destDir + '/img/'));

});

gulp.task('svg-sprite', function () {
    return gulp.src('src/{img,images}/svg/*.svg')
        .pipe(plumber({errorHandler: notify.onError({title: "svg-sprite"})}))
        .pipe(svgSprite({
            mode: {
                stack: true
            },
            svg: {
                xmlDeclaration: false,
                doctypeDeclaration: false,
                namespaceClassnames: false,
                namespaceIDs: false
            }
        }))
        .pipe(gulp.dest(config.destDir + '/img/icons/'));
});

gulp.task('fonts', function () {
    return gulp.src(['src/fonts/*.*', 'src/fonts/**/*.*'])
        .pipe(gulp.dest('dist/fonts'));
});


gulp.task('build', gulp.series('clean', 'sprite-create', gulp.parallel('styles', 'images', 'svg-sprite', 'java-script', 'fonts')));

gulp.task('watch', function () {
    gulp.watch(['src/scss/*.scss', 'src/scss/pages/*.scss', 'src/scss/components/**/*.scss', 'src/scss/foundation/**/*.scss'], gulp.series('styles'));
    gulp.watch(['src/img/*.{jpg,png}', 'src/img/**/*.{jpg,png}', '!src/{img,images}/svg/'], gulp.series('images'));
    gulp.watch('src/{img,images}/svg/*.svg', gulp.series('svg-sprite'));
    gulp.watch('src/img/sprite/*.png', gulp.series('sprite-create'));
    gulp.watch('src/{js,script,java}/**/*.js', gulp.series('java-script'));
    gulp.watch('src/fonts/*.{ttf,woff,woff2,eot}', gulp.series('fonts'));
});


gulp.task('dev', gulp.series('build', 'watch'));